classdef PersonalizedHRTFsLocalizationTest_exported < matlab.apps.AppBase

    % Properties that correspond to app components
    properties (Access = public)
        Figure                          matlab.ui.Figure
        StartTestButton                 matlab.ui.control.Button
        ReadyLampLabel                  matlab.ui.control.Label
        ReadyLamp                       matlab.ui.control.Lamp
        PersonalizedHRTFstestingappLabel  matlab.ui.control.Label
        LocalizationtestElevationLabel  matlab.ui.control.Label
        NextButton                      matlab.ui.control.Button
        BlocknumberListBoxLabel         matlab.ui.control.Label
        BlocknumberListBox              matlab.ui.control.ListBox
        StimolonumberListBoxLabel       matlab.ui.control.Label
        StimolonumberListBox            matlab.ui.control.ListBox
        esternalizzazione               matlab.ui.container.Panel
        PosizionestimoloSliderLabel     matlab.ui.control.Label
        SliderExt                       matlab.ui.control.Slider
        Image2                          matlab.ui.control.Image
        Image                           matlab.ui.control.Image
        info_label                      matlab.ui.control.Label
        id_label                        matlab.ui.control.Label
    end

    
    properties (Access = public)
        DialogApp   % Dialog box app
        measOk = 0; % flag acquisizione dati arduino
        arduino;
        no_arduino = true; %is there an arduino connected?
        
        audio_path = './data/stimoli/convolved_stimuli_id1/'; %convolved audio stimuli folder path
        save_path = './data/results/'
        
        numMeas = 36*6; %number of coordinates to test (36*3)*2
        countMeas = 0;
        countStimolo = 1;
        numHRTF = 3; %number of hrtfs blocks
        hrtfBlock = [];
        hrtfBlockSize = 36; %number of stimoli per block of hrtf
        listHRTF_id = []; %hrtf block number (0-> supervised, 1-> semi-supervised, 2-> KEMAR)
        listHRTF_idRep = []; %hrtf block number for second time rep
        hrtfId = 0; %what hrtf block am I using?
        randPos = []; %array of 36 random number for coordinates in one hrtf block
        
        beep = audioread('./data/beep.wav');
        
        id; %tester id (from 1 to...)
        sex; %tester sex (string)
        age; %tester age
        problem; %hearing problems?
        expert; %tester experience level
        stimoliDataType; %stimoli in formato .mat o . wav?
    end
    
    methods (Access = private) 
        function readSensorData(app, src, ~)
            % Read the ASCII data from the serialport object.
            data = readline(src);
            % Convert the string data to numeric type and save it in the UserData
            % property of the serialport object.
            src.UserData.Data(src.UserData.measCount, src.UserData.Count) = str2double(data);
            % Update the Count value of the serialport object.
            src.UserData.Count = src.UserData.Count + 1;
            % If 3 angles has been read:
            if src.UserData.Count > 3
                src.UserData.Count = 1;
                src.UserData.measCount = src.UserData.measCount + 1;
                app.measOk = 1;
                app.SliderExt.Enable = true;
                sound(app.beep, 44100);
                configureCallback(app.arduino,"off");
            end
        end
    end
    
    methods (Access = public)
        function updateUsrData(app, id, sex, age, problem, expert, stimoliDataType) %aggiungere "problemi udito" + "livello preparazione ascolto musicale"
            app.id = id;
            app.sex = sex;
            app.age = age;
            app.problem = problem;
            app.expert = expert;
            app.stimoliDataType = stimoliDataType;
        end
    end  

    % Callbacks that handle component events
    methods (Access = private)

        % Code that executes after component creation
        function startupFcn(app)
            addpath(genpath('.'));
            
            app.DialogApp = DialogAppForm(app); %open user data form
            
            waitfor(app.DialogApp);
            
            if(seriallist()~='')
                app.no_arduino = false;
                app.arduino = serialport("COM5", 115200); %porta seriale arduino
                configureTerminator(app.arduino,"CR/LF");
                flush(app.arduino);
                app.arduino.UserData = struct("Data",[],"Count",1, "measCount",1, "Extern",[], "Id",app.id, "Sex",app.sex, "Age",app.age, "HearingProblems",app.problem, "Experience",app.expert, "StimoloDataType",app.stimoliDataType, "Stimolo",[]);
            else
                app.StartTestButton.Enable = false;
                app.info_label.FontColor = 'red';
                app.info_label.Text = 'NO Arduino found!';
            end
            
            app.id_label.Text = ['Test for Id n.', mat2str(app.id)];
            
            %Latin Square order based on user_id
            for i=(0:3)
                app.listHRTF_id(1,i+1) = mod(app.id + i, app.numHRTF);
                app.listHRTF_idRep(1,i+1) = mod((app.id + 1 + i), app.numHRTF);         
            end
            
            
        end

        % Button pushed function: StartTestButton
        function StartTestButtonPushed(app, event)
            %hai ascoltato tutti gli stimoli previsti dal test?
            if((app.countMeas) < app.numMeas)
                configureCallback(app.arduino,"terminator", @app.readSensorData);
                app.StartTestButton.Enable = false;
                app.ReadyLamp.Color = "red";
                app.ReadyLampLabel.Text = "Testing...";
                app.StimolonumberListBox.Value = mat2str(mod(app.countMeas, app.hrtfBlockSize));
                
                %first or second time listening? 
                if(app.countMeas ~=0 && mod(app.countMeas,app.hrtfBlockSize * app.numHRTF) == 0)
                    app.hrtfBlock = app.listHRTF_idRep;
                    app.hrtfId = 0;
                elseif(app.countMeas ==0 && mod(app.countMeas,app.hrtfBlockSize * app.numHRTF) == 0)
                    app.hrtfBlock = app.listHRTF_id;
                    app.hrtfId = 0;
                end
                
               %fill randPos array every hrtfBlockSize
                if(mod(app.countMeas,app.hrtfBlockSize) == 0)
                    app.randPos = randperm(app.hrtfBlockSize);
                    app.countStimolo = 1;
                    app.hrtfId = app.hrtfId + 1;
                    app.BlocknumberListBox.Value = mat2str(app.hrtfId);
                end
                
                %sound stimuli with HRTFs
                %-------------DEBUG--------------
                %app.info_label.Text = [mat2str(app.hrtfBlock(1,app.hrtfId)), '_', mat2str(app.randPos(1,app.countStimolo)), app.stimoliDataType];
                %--------------------------------
                if(app.stimoliDataType == '.mat') %check for stimoli data type: ONLY FOR EXPERT USERS!!!
                    y = load([app.audio_path, mat2str(app.hrtfBlock(1,app.hrtfId)), ' - ', mat2str(app.randPos(1,app.countStimolo)) '.mat']);
                    sound(y.X_Hp, y.fs);
                else
                    [y, fs] = audioread([app.audio_path, mat2str(app.hrtfBlock(1,app.hrtfId)), '_', mat2str(app.randPos(1,app.countStimolo)) '.wav']);
                    sound(y, fs);
                end
                
                %storing stimolo number
                app.arduino.UserData.Stimolo(app.arduino.UserData.measCount, 1) = app.hrtfBlock(1,app.hrtfId);
                app.arduino.UserData.Stimolo(app.arduino.UserData.measCount, 2) = app.randPos(1,app.countStimolo);
                
                app.countStimolo = app.countStimolo + 1;
                app.countMeas = app.countMeas + 1;
                
            else
                app.info_label.FontColor = 'green';
                app.info_label.Text = 'Test completed!';
                app.StartTestButton.Enable = false;
            end
        end

        % Button pushed function: NextButton
        function NextButtonPushed(app, event)
            app.ReadyLamp.Color = "green";
            app.ReadyLampLabel.Text = "Ready...";
            app.NextButton.Enable = false;
            app.SliderExt.Enable = false;
            app.StartTestButton.Enable = true;
        end

        % Value changed function: SliderExt
        function SliderExtValueChanged(app, event)
            value = app.SliderExt.Value;
            app.arduino.UserData.Extern(app.arduino.UserData.measCount,1) = value;
            app.NextButton.Enable = true;
        end

        % Close request function: Figure
        function FigureCloseRequest(app, event)
            selection = questdlg('Close This App an save data?',...
'Close Request Function',...
      'Yes','No','Yes'); 
   switch selection 
      case 'Yes'
        if (~app.no_arduino && app.arduino.UserData.measCount ~= 1)
            assignin('base','UsrData', app.arduino.UserData);
            s1 = app.arduino.UserData;
            save([app.save_path, 'id_',mat2str(app.id),'.mat'], '-struct', 's1');
        end
        delete(app.DialogApp)
        delete(app)
      case 'No'
      return 
   end
        end
    end

    % Component initialization
    methods (Access = private)

        % Create UIFigure and components
        function createComponents(app)

            % Create Figure and hide until all components are created
            app.Figure = uifigure('Visible', 'off');
            app.Figure.Position = [100 100 640 480];
            app.Figure.Name = 'Personalized HRTFs - Localization test';
            app.Figure.CloseRequestFcn = createCallbackFcn(app, @FigureCloseRequest, true);

            % Create StartTestButton
            app.StartTestButton = uibutton(app.Figure, 'push');
            app.StartTestButton.ButtonPushedFcn = createCallbackFcn(app, @StartTestButtonPushed, true);
            app.StartTestButton.BackgroundColor = [1 0 0];
            app.StartTestButton.FontName = 'Corbel';
            app.StartTestButton.FontSize = 15;
            app.StartTestButton.FontWeight = 'bold';
            app.StartTestButton.FontColor = [1 1 1];
            app.StartTestButton.Position = [58 113 100 34];
            app.StartTestButton.Text = 'Start Test';

            % Create ReadyLampLabel
            app.ReadyLampLabel = uilabel(app.Figure);
            app.ReadyLampLabel.HorizontalAlignment = 'right';
            app.ReadyLampLabel.Position = [26 14 49 22];
            app.ReadyLampLabel.Text = 'Ready...';

            % Create ReadyLamp
            app.ReadyLamp = uilamp(app.Figure);
            app.ReadyLamp.Position = [90 14 20 20];

            % Create PersonalizedHRTFstestingappLabel
            app.PersonalizedHRTFstestingappLabel = uilabel(app.Figure);
            app.PersonalizedHRTFstestingappLabel.HorizontalAlignment = 'center';
            app.PersonalizedHRTFstestingappLabel.FontName = 'Calibri';
            app.PersonalizedHRTFstestingappLabel.FontSize = 20;
            app.PersonalizedHRTFstestingappLabel.Position = [191 434 259 27];
            app.PersonalizedHRTFstestingappLabel.Text = 'Personalized HRTFs testing app';

            % Create LocalizationtestElevationLabel
            app.LocalizationtestElevationLabel = uilabel(app.Figure);
            app.LocalizationtestElevationLabel.HorizontalAlignment = 'center';
            app.LocalizationtestElevationLabel.FontName = 'Calibri';
            app.LocalizationtestElevationLabel.FontSize = 18;
            app.LocalizationtestElevationLabel.Position = [219 412 204 23];
            app.LocalizationtestElevationLabel.Text = 'Localization test - Elevation';

            % Create NextButton
            app.NextButton = uibutton(app.Figure, 'push');
            app.NextButton.ButtonPushedFcn = createCallbackFcn(app, @NextButtonPushed, true);
            app.NextButton.BackgroundColor = [0.3922 0.8314 0.0745];
            app.NextButton.FontWeight = 'bold';
            app.NextButton.FontColor = [0.9412 0.9412 0.9412];
            app.NextButton.Enable = 'off';
            app.NextButton.Position = [514 113 100 34];
            app.NextButton.Text = 'Next';

            % Create BlocknumberListBoxLabel
            app.BlocknumberListBoxLabel = uilabel(app.Figure);
            app.BlocknumberListBoxLabel.HorizontalAlignment = 'right';
            app.BlocknumberListBoxLabel.Position = [68 358 79 22];
            app.BlocknumberListBoxLabel.Text = 'Block number';

            % Create BlocknumberListBox
            app.BlocknumberListBox = uilistbox(app.Figure);
            app.BlocknumberListBox.Items = {'1', '2', '3', ''};
            app.BlocknumberListBox.Enable = 'off';
            app.BlocknumberListBox.Position = [162 308 100 74];
            app.BlocknumberListBox.Value = '1';

            % Create StimolonumberListBoxLabel
            app.StimolonumberListBoxLabel = uilabel(app.Figure);
            app.StimolonumberListBoxLabel.HorizontalAlignment = 'right';
            app.StimolonumberListBoxLabel.Position = [331 358 90 22];
            app.StimolonumberListBoxLabel.Text = 'Stimolo number';

            % Create StimolonumberListBox
            app.StimolonumberListBox = uilistbox(app.Figure);
            app.StimolonumberListBox.Items = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35'};
            app.StimolonumberListBox.Enable = 'off';
            app.StimolonumberListBox.Position = [436 308 100 74];
            app.StimolonumberListBox.Value = '0';

            % Create esternalizzazione
            app.esternalizzazione = uipanel(app.Figure);
            app.esternalizzazione.Position = [191 13 260 245];

            % Create PosizionestimoloSliderLabel
            app.PosizionestimoloSliderLabel = uilabel(app.esternalizzazione);
            app.PosizionestimoloSliderLabel.HorizontalAlignment = 'center';
            app.PosizionestimoloSliderLabel.Enable = 'off';
            app.PosizionestimoloSliderLabel.Position = [13 104 58 27];
            app.PosizionestimoloSliderLabel.Text = {'Posizione'; 'stimolo '};

            % Create SliderExt
            app.SliderExt = uislider(app.esternalizzazione);
            app.SliderExt.Limits = [0 1];
            app.SliderExt.MajorTicks = [0 1];
            app.SliderExt.MajorTickLabels = {'Interno', 'Esterno'};
            app.SliderExt.Orientation = 'vertical';
            app.SliderExt.ValueChangedFcn = createCallbackFcn(app, @SliderExtValueChanged, true);
            app.SliderExt.MinorTicks = [];
            app.SliderExt.Enable = 'off';
            app.SliderExt.Position = [85 42 3 150];

            % Create Image2
            app.Image2 = uiimage(app.esternalizzazione);
            app.Image2.Position = [159 145 87 100];
            app.Image2.ImageSource = 'externalization-removebg.png';

            % Create Image
            app.Image = uiimage(app.esternalizzazione);
            app.Image.Position = [177 1 52 51];
            app.Image.ImageSource = 'inside_the_head-removebg.png';

            % Create info_label
            app.info_label = uilabel(app.Figure);
            app.info_label.HorizontalAlignment = 'center';
            app.info_label.FontWeight = 'bold';
            app.info_label.Position = [26 158 124 57];
            app.info_label.Text = '';

            % Create id_label
            app.id_label = uilabel(app.Figure);
            app.id_label.FontWeight = 'bold';
            app.id_label.Position = [26 43 85 22];
            app.id_label.Text = '';

            % Show the figure after all components are created
            app.Figure.Visible = 'on';
        end
    end

    % App creation and deletion
    methods (Access = public)

        % Construct app
        function app = PersonalizedHRTFsLocalizationTest_exported

            % Create UIFigure and components
            createComponents(app)

            % Register the app with App Designer
            registerApp(app, app.Figure)

            % Execute the startup function
            runStartupFcn(app, @startupFcn)

            if nargout == 0
                clear app
            end
        end

        % Code that executes before app deletion
        function delete(app)

            % Delete UIFigure when app is deleted
            delete(app.Figure)
        end
    end
end