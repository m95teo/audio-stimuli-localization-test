# Audio stimuli localization test App

A MATLAB based environment to manage a localization test to evaluate the performances of personalized HRTFs estimated using individual anthropometry (pinna measures). It requires an head-tracker connected to serial port "COM5".

For the experiment, an Arduino Nano with one MPU6050 sensor is used as a head-tracker sending data through serial port.

The localization test proposes to the listener three kinds of HRTFs and, for each type of transfer function, 36 stimuli are reproduced throughout headphones (in this example Beyerdynamic DT770 Pro was used).
Coordinates are represented using the following angles:
    - Azimuth = [0, 60, 120, 180, 240, 330]
    - Elevation = [-50, -30, -10, 10, 30, 50]

This test aims to verify how individualized HRTFs can improve the localization of an audio stimulus reproduced in a virtual audio environment through headphones.

In the repository, you can find an installer for the MATLAB App as well as the source code of the entire application developed using the MATLAB App Designer tool.

## HRTF Format
All HRTFs are saved in the SOFA file format. Please refer to [SOFA documentation](https://www.sofaconventions.org/mediawiki/index.php/SOFA_(Spatially_Oriented_Format_for_Acoustics)) for information about how to use.
